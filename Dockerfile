FROM openjdk:11.0.10-jre

COPY target/demospringboot*.jar app.jar

EXPOSE 14000

ENTRYPOINT ["java","-jar","/app.jar"]

