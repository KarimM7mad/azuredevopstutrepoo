package com.example.demospringboot.greetingAPIs;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/greeting")
public interface LandingPageGreetingAPI {

    @RequestMapping(value = "/welcome", method = RequestMethod.GET, produces = {"application/json"})
    @CrossOrigin("*")
    public ResponseEntity<String> welcome(@RequestParam(value = "name", required = false) String name);

}
