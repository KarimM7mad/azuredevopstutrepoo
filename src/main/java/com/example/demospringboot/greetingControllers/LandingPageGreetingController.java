package com.example.demospringboot.greetingControllers;

import com.example.demospringboot.greetingAPIs.LandingPageGreetingAPI;
import com.example.demospringboot.greetingServices.LandingPageGreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LandingPageGreetingController implements LandingPageGreetingAPI {

    @Autowired
    private LandingPageGreetingService landingPageGreetingService;

    @Override
    public ResponseEntity<String> welcome(String name) {
        return ResponseEntity.ok(landingPageGreetingService.welcome(name));
    }
}
