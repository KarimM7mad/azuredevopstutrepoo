package com.example.demospringboot.greetingServices;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import orgg.independentgrp.independentproj.HelperClass;


@Service
public class LandingPageGreetingService {

    public String welcome(String name) {
        System.out.println("entered service");
        if (!StringUtils.hasText(name))
            name = "elll Hamada";
        else
            name = HelperClass.getAppName(name);
        System.out.println("Finished service");
        return "{\"name\":\"Ahlan ystaa," + name + "!\"}";


    }
}
